Scenarium run interface (for development)
-----------------------------------------

This application simply run scenarium, and permit to simply add develoment modules

See scenarium documentation...

Simply add in the module path the plugin you want to integrate.


Licence
-------

MPL-2


Build
-----

simply do :

```{.bash}
export JAVA_HOME=/usr/local/java/jdk-14/
export PATH=/usr/local/java/jdk-14/bin/:$PATH
ant
```

run the program:

```{.bash}
java --module-path ./out/ant/package/scenarium-run-0.6.0-dev.jar:./out/ant/dependency/ -m io.scenarium.core.run/io.scenarium.run.ScenariumRun --log-color --log-level=verbose --workspace ./workspace_test



java --module-path ./out/deploy/lib/ -m io.scenarium.run/io.scenarium.run.ScenariumRun --log-color --log-level=verbose --workspace=./workspace_test

java --module-path ./out/deploy/lib/ -m io.scenarium.run/io.scenarium.run.ScenariumRun --log-color --log-level=verbose --workspace=./out/deploy/

```


Deploy your application:

```
# generic scenarium modules
ant package-deploy
# Your packages
ant -f build_perso.xml package-deploy

# install the deploy
mkdir deploy-end/lib
cp -v `find out/deploy/lib/ -name *.jar` deploy-end/lib
cp -v `find out/deploy/modules/ -name *.jar` deploy-end/lib
cp -v ../spearhead/geotransforms/out/ant/package/geotransform-0.2.0-dev.jar deploy-end/lib
cp -v ../spearhead/euclid/out/ant/package/euclid-0.2.0-dev.jar deploy-end/lib

cd deploy-end/

export JAVA_HOME=./jdk-14.0.2-full/; export JAVAFX_HOME=./jdk-14.0.2-full/; export PATH=./jdk-14.0.2-full/bin/:$PATH
# to simple test:
java --module-path ./lib/ -m io.scenarium.run/io.scenarium.run.ScenariumRun --log-color --log-level=verbose

# Normal RUN:
java --module-path ./lib/ -m io.scenarium.run/io.scenarium.run.ScenariumRun --log-color --log-level=verbose xxxxxxxxxxx.pdfd
# replay data : (need force the speed to be sure the speed is correct)
java -Dscenarium.core.scheduler.speed=100 --module-path ./lib/ -m io.scenarium.run/io.scenarium.run.ScenariumRun --log-color --log-level=verbose xxxxxxxxxxx.pdfd



```





