
module io.scenarium.run {
	requires io.scenarium.gui.core;
	requires io.scenarium.logger;
	requires java.base;
	requires io.scenarium.core;
}