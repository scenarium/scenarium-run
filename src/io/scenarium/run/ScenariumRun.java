package io.scenarium.run;

import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.scenarium.core.Scenarium;
import io.scenarium.gui.core.ScenariumGui;
import io.scenarium.logger.Logger;
import io.scenarium.pluginManager.ModuleManager;
import io.scenarium.run.internal.Log;

public class ScenariumRun {
	private ScenariumRun() {}

	public static void usage() {
		String applicationName = ScenariumRun.class.getCanonicalName();
		Log.print(applicationName + " - help : ");
		Log.print("  " + applicationName + " [options] [log] [file]");
		Log.print("");
		Log.print("    [options]:    Generic options");
		Log.print("        -h/--help:               Display this help");
		Log.print("        -r/--run:                Start the current scenario");
		Log.print("        -w=XXX/--workspace=XXX:  Specify a wokspace");
		Log.print("        -g/--no-gui:             Disable the GUI");
		Log.print("");
		Logger.usage();
		Log.print("    [file]:       Scenario to load at startup (load previous otherwise)");
		Log.print("");
		Log.print("    example:");
		Log.print("        " + applicationName + " --log-color --log-level=verbose --log-lib=sc-gui-core:none");
	}
	
	public static void main(String[] inputArgument) {
		try {
			List<String> args = new ArrayList<>(Arrays.asList(inputArgument));
			// Need to initialize logger first ...
			Logger.init(args);
			String workspace = null;
			boolean runWithNoGui = false;
			for (int iii = 0; iii < args.size(); ++iii) {
				String data = args.get(iii);
				if (data.contentEquals("-h") || data.contentEquals("--help")) {
					usage();
					System.exit(0);
				} else if (data.contentEquals("-v") || data.contentEquals("--version")) {
					Log.print("Scenarium version:" + Scenarium.VERSION);
					System.exit(0);
				} else if (data.contentEquals("-r") || data.contentEquals("--run")) {
					Scenarium.setRunAtStartUp(true);
				} else if (data.startsWith("-w="))
					workspace = data.substring(3);
				else if (data.startsWith("--workspace=")) {
					workspace = data.substring(12);
				} else if (data.contentEquals("-g") || data.contentEquals("--no-gui")) {
					runWithNoGui = true;
				} else if (data.startsWith("-"))
					Log.print("Unknow application argument: '" + data + "'");
				else {
					Log.info("must be the scenario to run: " + data);
					Scenarium.setUserScenarioFile(data);
				}
			}
			Log.verbose("init module manager");
			if (workspace != null) {
				Scenarium.setWorkspace(workspace);
				// we load first all the modules; otherwise we can not start.
				ModuleManager.birth(workspace + FileSystems.getDefault().getSeparator() + ModuleManager.MODULES_GENERIC_FOLDER_NAME);
			} else
				// we load first all the modules; otherwise we can not start.
				ModuleManager.birth();
			
			Log.verbose("init scenarium gui");
			//String[] argsForGui = args.toArray(new String[0]);
			String[] argsForGui = new String[0];
			if (runWithNoGui) {
				Log.debug("Start Application !! ");
				Scenarium.run(argsForGui);
				Log.debug("Stop Application !! ");
			} else {
				Log.debug("Start GUI Application !! ");
				ScenariumGui.run(argsForGui);
				Log.debug("Stop GUI Application !! ");
			}
			
			Log.todo("un-init all the plugin ... ");
		} catch (Exception ex) {
			Log.error("catch error in main: " + ex.getMessage());
			ex.printStackTrace();
		}
	}
}
